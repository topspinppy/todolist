import { task } from '../repository/index'

async function listTaskByID(ctx) {
  let { id } = ctx.params
  let t = await task.queryByID(id)

  ctx.body = {
    code: 200,
    data: t,
    msg: 'Query task by ID successfully',
  }
}

export default listTaskByID
