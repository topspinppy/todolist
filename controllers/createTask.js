import { task } from '../repository/index'

function createTasks(ctx) {
  let reqBody = ctx.request.body
  task.create(reqBody)
  ctx.body = {
    code: 200,
    data: {},
    msg: 'Create a task successfully',
  }
}

export default createTasks
