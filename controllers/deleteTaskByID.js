import { task } from '../repository/index'

async function deleteTaskByID(ctx) {
  let id = ctx.params.id

  task.deleteByID(id)
  let t = await task.queryByID(id)
  ctx.body = {
    code: 200,
    data: t,
    msg: 'Delete task by ID successfully',
  }
}

export default deleteTaskByID
