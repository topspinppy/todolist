import { task } from '../repository/index'

async function listAllTasks(ctx) {
  let allTasks = await task.queryAll()
  ctx.body = {
    code: 200,
    data: { allTasks },
    msg: 'Query all tasks successfully',
  }
}

export default listAllTasks
