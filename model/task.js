import mongoose from 'mongoose'

// Define a schema
let Schema = mongoose.Schema

// Schema blueprint
const schema = new Schema(
  {
    title: String,
    description: String,
  },
  { timestamps: { createdAt: 'createdAt' } },
)

let Task = mongoose.model('Task', schema)

export { Task }
