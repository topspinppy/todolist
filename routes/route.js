import Router from 'koa-router' 
import * as controller from '../controllers/index'

// New router instance
const router = new Router()

router.get('/', (ctx) => {
  ctx.status = 200
  ctx.body = {
    msg: 'Hello world',
  }
})

router.get('/api/tasks', controller.listAllTasks)
  .get('/api/tasks/:id', controller.listTaskByID)
  .post('/api/tasks', controller.createTask)
  .patch('/api/tasks/:id', controller.updateTaskByID)
  .delete('/api/tasks/:id', controller.deleteTaskByID)

export default router
